package com.tiiracks.eazenet.core.network;

import java.lang.reflect.Method;

/**
 * Created by Paul Cech on 15.06.16.
 */
public interface INetwork {

    /**
     *
     * @param _baseUrl
     */
    void setBaseUrl( String _baseUrl );

    /**
     *
     * @param _callback
     */
    void setKeyTimestampCallback( IKeyTimestampCallback _callback );

    /**
     *
     * @param _o
     * @param _method
     * @param _methodParameters
     * @return
     * @throws Throwable
     */
    BaseRequest onMethodInvocationCalled( Object _o, Method _method, Object[] _methodParameters ) throws Throwable;

    /**
     *
     * @param _baseRequest
     */
    void startRequest( BaseRequest _baseRequest );
}

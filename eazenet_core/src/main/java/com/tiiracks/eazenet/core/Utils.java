package com.tiiracks.eazenet.core;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by Paul Cech on 16.06.16.
 */
public class Utils {

    // ----------------------------------------------------------------------------
    // Member
    // ----------------------------------------------------------------------------

    private static final String LOG_TAG = Utils.class.getSimpleName();


    // ----------------------------------------------------------------------------
    // Public methods
    // ----------------------------------------------------------------------------

    /**
     * Needs the Serializeable interface
     *
     * @param _object
     * @return
     * @throws IOException
     */
    public static byte[] serializeObject( Object _object ) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ObjectOutputStream    os  = new ObjectOutputStream( out );
        os.writeObject( _object );
        return out.toByteArray();
    }

    /**
     *  Needs the Serializeable interface
     *
     * @param _objectData
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static Object deserializeObject( byte[] _objectData ) throws IOException, ClassNotFoundException {
        ByteArrayInputStream in = new ByteArrayInputStream( _objectData );
        ObjectInputStream    is = new ObjectInputStream( in );
        return is.readObject();
    }


    // ----------------------------------------------------------------------------
    // Private methods
    // ----------------------------------------------------------------------------

    // ----------------------------------------------------------------------------
    // Listener
    // ----------------------------------------------------------------------------


}

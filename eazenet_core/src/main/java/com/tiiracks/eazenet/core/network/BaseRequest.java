package com.tiiracks.eazenet.core.network;

/**
 * Created by Paul Cech on 18.08.16.
 */
public class BaseRequest
{

    // ----------------------------------------------------------------------------
    // Member
    // ----------------------------------------------------------------------------

    private static final String LOG_TAG = BaseRequest.class.getSimpleName();

    protected String mKey;

    /**
     * The payload which should be sent to the server.
     * When the network interface creates a request, the payload member stores a normal java POJO as Object.
     * As soon as the core module calls the {@link INetwork#startRequest(BaseRequest)} method, the
     * mPayload no longer holds a POJO but instead a string encoded as a byte array.
     */
    protected Object mPayload;

    protected Class< ? > mPayloadClass;

    protected boolean mTimestampSet;

    // ----------------------------------------------------------------------------
    // Constructor
    // ----------------------------------------------------------------------------

    public BaseRequest()
    {
        mKey = "";
        mPayload = null;
        mTimestampSet = false;
    }

    // ----------------------------------------------------------------------------
    // Public methods
    // ----------------------------------------------------------------------------

    public String getKey() {
        return mKey;
    }

    public void setKey( String _key ) {
        mKey = _key;
    }

    public Object getPayload() {
        return mPayload;
    }

    public void setPayload( Object _payload ) {
        mPayload = _payload;
    }

    public Class< ? > getPayloadClass() {
        return mPayloadClass;
    }

    public void setPayloadClass( Class< ? > _payloadClass ) {
        mPayloadClass = _payloadClass;
    }

    public boolean isTimestampSet() {
        return mTimestampSet;
    }

    public void setTimestampSet( boolean _timestampSet ) {
        mTimestampSet = _timestampSet;
    }

    // ----------------------------------------------------------------------------
    // Private methods
    // ----------------------------------------------------------------------------

    // ----------------------------------------------------------------------------
    // Listener
    // ----------------------------------------------------------------------------


}

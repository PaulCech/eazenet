package com.tiiracks.eazenet.core.event;

import java.lang.reflect.Method;
import java.util.Map;

/**
 * Created by Paul Cech on 24.06.16.
 */
public class RegisteredClass {

    // ----------------------------------------------------------------------------
    // Member
    // ----------------------------------------------------------------------------

    private static final String LOG_TAG = RegisteredClass.class.getSimpleName();

    private Object mRegisteredClass;

    private Map<Object, RegisteredMethod> mMethodMap;

    // ----------------------------------------------------------------------------
    // Constructor
    // ----------------------------------------------------------------------------

    public RegisteredClass() {
    }

    public RegisteredClass( Object _registeredClass, Map< Object, RegisteredMethod > _methodMap ) {
        mRegisteredClass = _registeredClass;
        mMethodMap = _methodMap;
    }

    // ----------------------------------------------------------------------------
    // Public methods
    // ----------------------------------------------------------------------------

    public Object getRegisteredClass() {
        return mRegisteredClass;
    }

    public void setRegisteredClass( Object _registeredClass ) {
        mRegisteredClass = _registeredClass;
    }

    public Map< Object, RegisteredMethod > getMethodMap() {
        return mMethodMap;
    }

    public void setMethodMap( Map< Object, RegisteredMethod > _methodMap ) {
        mMethodMap = _methodMap;
    }


    // ----------------------------------------------------------------------------
    // Private methods
    // ----------------------------------------------------------------------------

    // ----------------------------------------------------------------------------
    // Listener
    // ----------------------------------------------------------------------------


}

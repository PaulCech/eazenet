package com.tiiracks.eazenet.core;

import android.os.Looper;
import android.util.Log;

import com.tiiracks.eazenet.core.convert.EConvertResult;
import com.tiiracks.eazenet.core.convert.IConverter;
import com.tiiracks.eazenet.core.event.EventPool;
import com.tiiracks.eazenet.core.merge.ObjectMerger;
import com.tiiracks.eazenet.core.network.BaseRequest;
import com.tiiracks.eazenet.core.network.ENetworkResult;
import com.tiiracks.eazenet.core.network.IKeyTimestampCallback;
import com.tiiracks.eazenet.core.network.ResponsePool;
import com.tiiracks.eazenet.core.network.INetwork;
import com.tiiracks.eazenet.core.network.BaseResponse;
import com.tiiracks.eazenet.core.persist.EPersistResult;
import com.tiiracks.eazenet.core.persist.IPersist;
import com.tiiracks.eazenet.core.persist.PersistData;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Created by Paul Cech on 15.06.16.
 */
public class EazeNet {
    // ----------------------------------------------------------------------------
    // Member
    // ----------------------------------------------------------------------------

    private static final String LOG_TAG = EazeNet.class.getSimpleName();

    private INetwork   mNetwork;
    private IConverter mConverter;
    private IPersist   mPersist;

    private String mBaseUrl;

    // ----------------------------------------------------------------------------
    // Constructor
    // ----------------------------------------------------------------------------


    public EazeNet( INetwork _network, IConverter _converter, IPersist _persist, String _baseUrl ) {
        mNetwork = _network;
        mConverter = _converter;
        mPersist = _persist;
        mBaseUrl = _baseUrl;

        mNetwork.setBaseUrl( mBaseUrl );
        mNetwork.setKeyTimestampCallback( mKeyTimestampCallback );

        ResponsePool.getInstance().setPoolListener( mPoolListener );
    }

    // ----------------------------------------------------------------------------
    // Public methods
    // ----------------------------------------------------------------------------

    @SuppressWarnings( "unchecked" )
    public < T > T create( final Class< T > _service ) {
        // TODO: Check interface for validity
        return ( T ) Proxy.newProxyInstance(
                _service.getClassLoader(),
                new Class< ? >[]{ _service },
                new InvocationHandler() {
                    @Override
                    public Object invoke( Object _o, Method _method, Object[] _methodParameters ) throws Throwable {

                        if ( _method.getDeclaringClass() == Object.class ) {
                            return _method.invoke( this, _methodParameters );
                        }

                        BaseRequest request = mNetwork.onMethodInvocationCalled( _o, _method, _methodParameters );

                        if ( request.getPayload() != null && !request.isTimestampSet() ) {
                            long timestamp = mPersist.getTimestampFromKey( request.getKey() );
                            Object injectedObject = ObjectMerger.injectTimestampToObject( timestamp, request );

                            byte[] convertedPayload = mConverter.convertToByte( injectedObject );

                            request.setPayload( convertedPayload );
                        }

                        mNetwork.startRequest( request );

                        return null;
                    }
                }
        );
    }

    public void register( Object _o ) {
        EventPool.getInstance().register( _o );
    }

    public void unregister( Object _o ) {
        EventPool.getInstance().unregister( _o );
    }

    // ----------------------------------------------------------------------------
    // Private methods
    // ----------------------------------------------------------------------------

    private void handleIncomingResponse( BaseResponse _response ) {

        Runnable responseRunnable = new Runnable() {
            @Override
            public void run() {
                if ( _response.getNetworkResult() == ENetworkResult.AVAILABLE ) {
                    handleSuccessfulResponse( _response );
                } else if ( _response.getNetworkResult() == ENetworkResult.UNREACHABLE ) {
                    // No Network connection was detected. Try to load old result
                    handleNoNetwork( _response );
                }
            }
        };

        // Check if the network module has opened a new thread so the coming operations
        // won't run on the main thread. If the thread is other than the main thread, just continue.
        // If it is the main thread, create a new thread.
        if ( Looper.myLooper() == Looper.getMainLooper() ) {

            // Every response handling is done in its own thread. When the work is done,
            // the EventPool automatically sends the result to the subscribed thread.
            Thread responseThread = new Thread( responseRunnable );
            responseThread.start();
        } else {

            // Run the runnable in the current thread, as we are already on different thread than
            // the main thread.
            responseRunnable.run();
        }
    }

    private void handleSuccessfulResponse( BaseResponse _response ) {
        Object resultConverted = mConverter.convertToObject( _response.getData(), _response.getDataClass() );

        if ( resultConverted != null ) {

            Result      finalResult = new Result( _response.getNetworkResult(), EPersistResult.ERROR, EConvertResult.SUCCESSFUL );
            PersistData oldData     = mPersist.load( _response.getKey() );
            Object      mixedObject = null;

            if ( oldData != null ) {
                Object oldDataConverted = mConverter.convertToObject( oldData.getData(), _response.getDataClass() );

                if ( oldDataConverted != null ) {
                    mixedObject = ObjectMerger.merge( resultConverted, oldDataConverted );
                    finalResult.setPersistResult( EPersistResult.UPDATED );
                }
            } else {
                mixedObject = resultConverted;
                finalResult.setPersistResult( EPersistResult.CREATED );
            }

            if ( mixedObject != null ) {

                // Check if a timestamp was delivered by the network interface. If not
                // check the class, if any member has the timestamp annotation. If there is no
                // annotation, just use the current timestamp.
                long timestamp = _response.getTimestamp();
                if ( timestamp == Const.UNSET_TIMESTAMP ) {
                    timestamp = ObjectMerger.getTimestampFromObject( mixedObject );

                    if ( timestamp == Const.UNSET_TIMESTAMP ) {
                        // TODO: Still no timestamp. For now leave it like that. As soon as something occurs, where we have to use the current timestamp from the system.
//                        timestamp = System.currentTimeMillis();
                    }
                }

                byte[]      serializedObject = mConverter.convertToByte( mixedObject );
                PersistData newData          = new PersistData( serializedObject, timestamp );
                boolean     saveSuccess      = mPersist.save( _response.getKey(), newData );

                if ( saveSuccess ) {
                    EventPool.getInstance().postEventResult( mixedObject, finalResult );
                }
            }

        } else {
            Log.d( LOG_TAG, "handleIncomingResponse error" );
        }
    }

    private void handleNoNetwork( BaseResponse _response ) {
        PersistData data = mPersist.load( _response.getKey() );

        if ( data != null ) {
            // Data is available. Send it to all subscribers
            Object objectResult = mConverter.convertToObject( data.getData(), _response.getDataClass() );

            // TODO: Think about simplifying code
            if ( objectResult != null ) {
                // Loading was successful and conversion as well
                Result finalResult = new Result( _response.getNetworkResult(), EPersistResult.LOADED, EConvertResult.SUCCESSFUL );
                EventPool.getInstance().postEventResult( objectResult, finalResult );
            } else {
                // Loading was successful but the conversion broke
                Result finalResult = new Result( _response.getNetworkResult(), EPersistResult.LOADED, EConvertResult.ERROR );
                EventPool.getInstance().postEventResult( null, finalResult );
            }
        } else {
            // No data was previously stored. let the subscribers know, that there is no data available
            Result finalResult = new Result( _response.getNetworkResult(), EPersistResult.NO_DATA, EConvertResult.SKIPPED );
            EventPool.getInstance().postEventResult( null, finalResult );
        }
    }

    // ----------------------------------------------------------------------------
    // Pool Listener
    // ----------------------------------------------------------------------------

    private ResponsePool.IPoolListener mPoolListener = new ResponsePool.IPoolListener() {
        @Override
        public void onResponseAdded( BaseResponse _response ) {
            handleIncomingResponse( _response );
        }
    };

    // ----------------------------------------------------------------------------
    // Key Timestamp Listener
    // ----------------------------------------------------------------------------

    private IKeyTimestampCallback mKeyTimestampCallback = new IKeyTimestampCallback() {
        @Override
        public long getTimestampFromKey( String _key ) {
            return mPersist.getTimestampFromKey( _key );
        }
    };

    // ----------------------------------------------------------------------------
    // Builder
    // ----------------------------------------------------------------------------

    public static final class Builder {

        private INetwork   mNetwork;
        private IConverter mConverter;
        private IPersist   mPersist;
        private String     mBaseUrl;

        public void setNetwork( INetwork _network ) {
            mNetwork = _network;
        }

        public void setConverter( IConverter _converter ) {
            mConverter = _converter;
        }

        public void setPersistence( IPersist _persist ) {
            mPersist = _persist;
        }

        public void setBaseUrl( String _baseUrl ) {
            mBaseUrl = _baseUrl;
        }

        public EazeNet create() {

            if ( mNetwork == null ) {
                throw new IllegalStateException( "Network instance should not be null" );
            }


            if ( mConverter == null ) {
                throw new IllegalStateException( "Converter instance should not be null" );
            }


            if ( mPersist == null ) {
                throw new IllegalStateException( "Persistence instance should not be null" );
            }

            if ( mBaseUrl == null || mBaseUrl.isEmpty() ) {
                throw new IllegalStateException( "Base url should not be null" );
            }

            // TODO: check url for validity

            return new EazeNet( this.mNetwork, this.mConverter, this.mPersist, this.mBaseUrl );
        }
    }

}

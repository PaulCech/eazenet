package com.tiiracks.eazenet.core.event;

import android.os.Handler;
import android.os.Looper;

import com.tiiracks.eazenet.core.Result;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Paul Cech on 24.06.16.
 */
public class EventPool {
    // ----------------------------------------------------------------------------
    // Member
    // ----------------------------------------------------------------------------

    private static final String LOG_TAG = EventPool.class.getSimpleName();

    private ArrayList<RegisteredClass> mRegisteredClasses;
    private Handler                    mMainHandler;

    // ----------------------------------------------------------------------------
    // Constructor
    // ----------------------------------------------------------------------------

    private EventPool() {
        mRegisteredClasses = new ArrayList<>(  );
        mMainHandler = new Handler( Looper.getMainLooper() );
    }

    // ----------------------------------------------------------------------------
    // Singleton
    // ----------------------------------------------------------------------------

    private static EventPool sInstance;

    public static EventPool getInstance() {
        if ( sInstance == null ) {
            sInstance = new EventPool();
        }
        return sInstance;
    }

    // ----------------------------------------------------------------------------
    // Public methods
    // ----------------------------------------------------------------------------

    public void register( Object _o ) {
        if ( containsClass( _o ) ) {
            throw new IllegalArgumentException( "The same class cannot be added twice for events. Please unregister your class before adding again." );
        }
        else {
            if ( !addClass( _o ) ) {
                throw new IllegalArgumentException( "A registered event class needs a EazeCallback Annotation, to receive event callbacks" );
            }
        }
    }

    public void unregister( Object _o ) {
        if ( containsClass( _o ) ) {
            removeClass( _o );
        }
    }

    public void postEventResult( Object _data, Result _result) {
        for ( RegisteredClass registeredClass : mRegisteredClasses ) {

            RegisteredMethod registeredMethod = registeredClass.getMethodMap().get( _data.getClass() );

            if ( registeredMethod != null ) {

                Runnable mainRunnable = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            registeredMethod.getMethod().invoke( registeredClass.getRegisteredClass(), _data, _result );
                        } catch ( IllegalAccessException _e ) {
                            _e.printStackTrace();
                        } catch ( InvocationTargetException _e ) {
                            _e.printStackTrace();
                        }
                    }
                };

                // Check on which thread the event should be send
                if ( registeredMethod.getThread() == EThread.MAIN ) {
                    mMainHandler.post( mainRunnable );
                } else {
                    mainRunnable.run();
                }

            }
        }

    }

    // ----------------------------------------------------------------------------
    // Private methods
    // ----------------------------------------------------------------------------

    private boolean containsClass( Object _o ) {
        for ( RegisteredClass registeredClass : mRegisteredClasses ) {
            if ( registeredClass.getRegisteredClass().equals( _o ) ) {
                return true;
            }
        }
        return false;
    }

    private boolean addClass( Object _o ) {

        Map<Object, RegisteredMethod> eventMap = checkClassForAnnotations( _o );
        if ( eventMap != null ) {

            RegisteredClass registeredClass = new RegisteredClass( _o, eventMap );
            mRegisteredClasses.add( registeredClass );
            return true;
        }

        return false;
    }

    private boolean removeClass( Object _o ) {
        for ( RegisteredClass registeredClass : mRegisteredClasses ) {
            if ( registeredClass.getRegisteredClass().equals( _o ) ) {
                mRegisteredClasses.remove( registeredClass );
                return true;
            }
        }
        return false;
    }

    private Map<Object, RegisteredMethod> checkClassForAnnotations( Object _o ) {
        Map<Object, RegisteredMethod> result = null;

        Method[] methods = _o.getClass().getDeclaredMethods();
        for ( Method method : methods ) {
            if ( method.isAnnotationPresent( EazeCallback.class ) ) {

                Class<?>[] parameters = method.getParameterTypes();
                if ( parameters == null || parameters.length == 0 ) {
                    throw new IllegalArgumentException( "Registered event methods need an object parameter of the type which is requested." );
                }

                if ( result == null ) {
                    result = new HashMap<>(  );
                }

                EazeCallback eazeCallback = method.getAnnotation( EazeCallback.class );

                // The first parameter of the method should always be the type of class which is requested for events.
                // The second parameter always represents the internal result.
                result.put( parameters[0], new RegisteredMethod( method, eazeCallback.thread() ) );
            }
        }

        return result;
    }

    // ----------------------------------------------------------------------------
    // Listener
    // ----------------------------------------------------------------------------


}

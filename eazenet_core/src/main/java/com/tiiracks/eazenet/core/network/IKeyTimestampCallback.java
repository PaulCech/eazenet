package com.tiiracks.eazenet.core.network;

/**
 * Created by Paul Cech on 28.06.16.
 */
public interface IKeyTimestampCallback {

    /**
     *
     * @param _key
     * @return
     */
    long getTimestampFromKey( String _key );
}

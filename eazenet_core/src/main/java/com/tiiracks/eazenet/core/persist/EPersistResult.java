package com.tiiracks.eazenet.core.persist;

/**
 * Created by Paul Cech on 15.06.16.
 */
public enum EPersistResult {
    /**
     * An error has happened while trying to load or save a response.
     */
    ERROR,

    /**
     * A new entry has been added to the persistence module under the referenced key.
     */
    CREATED,

    /**
     * An existing entry has been found and updated with new data.
     */
    UPDATED,

    /**
     * This result will only be received, when no network connection is available and data has already
     * been stored under the referenced key.
     */
    LOADED,

    /**
     * This result will only be received, when no network connection is available and no data has
     * been stored under the referenced key.
     */
    NO_DATA
}

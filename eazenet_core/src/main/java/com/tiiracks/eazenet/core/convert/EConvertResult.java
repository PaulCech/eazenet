package com.tiiracks.eazenet.core.convert;

/**
 * Created by Paul Cech on 07.07.16.
 */
public enum EConvertResult {
    /**
     * Is used when the conversion of a response to an POJO was successful.
     */
    SUCCESSFUL,

    /**
     * Is used when the conversion somehow broke or was not successful.
     */
    ERROR,

    /**
     * Is used when no conversion was done, as no data was available.
     */
    SKIPPED
}

package com.tiiracks.eazenet.core.event;

/**
 * Created by Paul Cech on 07.07.16.
 */
public enum EThread {
    /**
     * Subscribed methods with the MAIN ThreadMode will be called on the main thread.
     */
    MAIN,

    /**
     * Subscribed methods will be called on the thread which is given by the posting event.
     */
    POSTING
}

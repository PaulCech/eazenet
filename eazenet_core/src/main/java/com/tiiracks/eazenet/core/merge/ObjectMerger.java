package com.tiiracks.eazenet.core.merge;

import android.util.Log;

import com.tiiracks.eazenet.core.Const;
import com.tiiracks.eazenet.core.network.BaseRequest;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

/**
 * Created by Paul Cech on 12.07.16.
 */
public class ObjectMerger {

    // ----------------------------------------------------------------------------
    // Member
    // ----------------------------------------------------------------------------

    private static final String LOG_TAG = ObjectMerger.class.getSimpleName();

    // ----------------------------------------------------------------------------
    // Constructor
    // ----------------------------------------------------------------------------

    public ObjectMerger() {
    }

    // ----------------------------------------------------------------------------
    // Public methods
    // ----------------------------------------------------------------------------

    public static Object merge( Object _newData, Object _oldData ) {

        /**
         * Check if is array or list
         * if array or list: check type to see if it is an Updateable list.
         * If it is a normal object, then check the object itself for Updateable
         */

        Class< ? > dataClass    = _oldData.getClass();
        boolean    isUpdateable = false;
        boolean    isArray      = dataClass.isArray();
        boolean    isCollection = AnnotationProcessor.isCollection( _newData );
        Object     result       = _newData;

        if ( isArray ) {

            Class< ? > arrayClass = dataClass.getComponentType();
            isUpdateable = AnnotationProcessor.isUpdateable( arrayClass );

        } else if ( isCollection ) {

            // TODO: Implement collections

        } else {
            // Is normal object
            isUpdateable = AnnotationProcessor.isUpdateable( dataClass );
        }

        if ( isUpdateable ) {

            if ( isArray ) {

                result = mergeArrays( _newData, _oldData );

            } else if ( isCollection ) {

                // TODO: Implement collections

            } else {

                boolean hasMixableField = false;

                for ( Field field : dataClass.getDeclaredFields() ) {

                    if ( field.getAnnotation( Mixable.class ) != null ) {
                        hasMixableField = true;

                        field.setAccessible( true );

                        try {

                            Object newObject = field.get( _newData );
                            Object oldObject = field.get( _oldData );
                            Object merged    = merge( newObject, oldObject );
                            field.set( result, merged );

                        } catch ( IllegalAccessException _e ) {
                            _e.printStackTrace();
                        }
                    }
                }
            }

        }

        return result;
    }

    /**
     * @param _object
     * @return
     */
    public static long getTimestampFromObject( Object _object ) {
        long timestamp = Const.UNSET_TIMESTAMP;

        Field  resultField  = null;
        Object resultObject = _object;

        if ( _object.getClass().isArray() ) {

            Class< ? > arrayClass     = _object.getClass().getComponentType();
            Field      timestampField = AnnotationProcessor.getTimestampField( arrayClass );
            Object[]   arrayObject    = ( Object[] ) _object;

            if ( timestampField != null && arrayObject.length > 0 ) {
                resultField = timestampField;
                resultObject = arrayObject[ 0 ];
            }

        } else {
            Field timestampField = AnnotationProcessor.getTimestampField( _object.getClass() );

            if ( timestampField != null ) {
                resultField = timestampField;
            }
        }

        if ( resultField != null ) {
            resultField.setAccessible( true );
            if ( resultField.getType().isAssignableFrom( Long.TYPE ) ) {
                try {
                    timestamp = ( long ) resultField.get( resultObject );
                } catch ( IllegalAccessException _e ) {
                    _e.printStackTrace();
                }
            } else {
                throw new IllegalArgumentException( "The timestamp annotation needs to be assigned to a long object" );
            }
        }

        return timestamp;
    }

    /**
     * Injects a timestamp in the given object if there is a field with the Timestamp annotation.
     * If the value of the timestamp field is other than -1, then nothing will be injected.
     *
     * @param _timestamp
     * @return
     */
    public static Object injectTimestampToObject( long _timestamp, BaseRequest _request ) {

        Object     object      = _request.getPayload();
        Class< ? > objectClass = _request.getPayloadClass();

        for ( Field field : objectClass.getDeclaredFields() ) {

            Annotation annotation = field.getAnnotation( Timestamp.class );

            if ( annotation != null ) {
                if ( field.getType().isAssignableFrom( Long.TYPE ) ) {
                    field.setAccessible( true );
                    try {
                        // if the value is other than -1 don't inject the present timestamp
                        if ( ( ( long ) field.get( object ) ) == Const.UNSET_TIMESTAMP ) {
                            field.set( object, _timestamp );
                            _request.setTimestampSet( true );
                        }
                    } catch ( IllegalAccessException _e ) {
                        _e.printStackTrace();
                    }
                } else {
                    throw new IllegalArgumentException( "The timestamp annotation needs to be assigned to a long object" );
                }
                break;
            }
        }

        return object;
    }


    // ----------------------------------------------------------------------------
    // Private methods
    // ----------------------------------------------------------------------------

    /**
     * @param _newData
     * @param _oldData
     * @return
     */
    public static Object mergeArrays( Object _newData, Object _oldData ) {
        // TODO: Do we need a check if the data objects are arrays?
        Object[]            newDataArray = ( Object[] ) _newData;
        Object[]            oldDataArray = ( Object[] ) _oldData;
        int                 newLength    = newDataArray.length;
        int                 oldLength    = oldDataArray.length;
        ArrayList< Object > resultList   = null;

        // Check if an Id Annotation is present and remove duplicates or update them.
        Class< ? > arrayClass = newDataArray.getClass().getComponentType();
        Field      idField    = AnnotationProcessor.getIdField( arrayClass );

        if ( idField != null ) {

            idField.setAccessible( true );

            resultList = new ArrayList<>();
            Collections.addAll( resultList, newDataArray );

            for ( Object oldDataObj : oldDataArray ) {
                try {
                    Object idOld = idField.get( oldDataObj );

                    boolean isDuplicate = false;
                    for ( Object resultObj : resultList ) {

                        Object idNew = idField.get( resultObj );

                        if ( idNew.equals( idOld ) ) {
                            isDuplicate = true;
                            break;
                        }
                    }

                    if ( !isDuplicate ) {
                        resultList.add( oldDataObj );
                    }

                } catch ( IllegalAccessException _e ) {
                    _e.printStackTrace();
                }
            }
        }

        if ( resultList != null ) {
            // Copy contents from arraylist to array
            newDataArray = Arrays.copyOf( newDataArray, resultList.size() );
            for ( int index = 0; index < resultList.size(); index++ ) {
                newDataArray[ index ] = resultList.get( index );
            }
        } else {
            // Resize array by creating a new one with the contents of new data and giving it more space
            newDataArray = Arrays.copyOf( newDataArray, newLength + oldLength );
            System.arraycopy( oldDataArray, 0, newDataArray, newLength, oldLength );

        }

        return newDataArray;
    }


    // ----------------------------------------------------------------------------
    // Listener
    // ----------------------------------------------------------------------------


}

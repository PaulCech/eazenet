package com.tiiracks.eazenet.core.persist;

/**
 * Created by Paul Cech on 15.06.16.
 */
public class PersistData {

    // ----------------------------------------------------------------------------
    // Member
    // ----------------------------------------------------------------------------

    private static final String LOG_TAG = PersistData.class.getSimpleName();

    /**
     * The data which will be saved in the persistence module
     */
    private byte[] mData;

    /**
     *  The timestamp which represents the last time the data has changed
     */
    private long   mTimestamp;

    // ----------------------------------------------------------------------------
    // Constructor
    // ----------------------------------------------------------------------------

    public PersistData() {
    }

    public PersistData( byte[] _data, long _timestamp ) {
        mData = _data;
        mTimestamp = _timestamp;
    }

    public PersistData( byte[] _data, long _timestamp, EPersistResult _result ) {
        mData = _data;
        mTimestamp = _timestamp;
    }

    // ----------------------------------------------------------------------------
    // Public methods
    // ----------------------------------------------------------------------------


    public byte[] getData() {
        return mData;
    }

    public void setData( byte[] _data ) {
        mData = _data;
    }

    public long getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp( long _timestamp ) {
        mTimestamp = _timestamp;
    }
}

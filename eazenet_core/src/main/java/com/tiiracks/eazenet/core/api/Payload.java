package com.tiiracks.eazenet.core.api;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Paul Cech on 15.08.16.
 */
@Target( ElementType.PARAMETER )
@Retention( RetentionPolicy.RUNTIME )
@Documented
public @interface Payload {
    // Used to declare the data which should be send to the server.
    // As soon as the Payload annotation is used, the library will automatically look for a
    // Timestamp annotation in the object class and will inject the timestamp in the corresponding field.
}

package com.tiiracks.eazenet.core.persist;

/**
 * Created by Paul Cech on 15.06.16.
 */
public interface IPersist {

    /**
     * Saves the provided data under the given key. The way how the data is saved depends on the
     * implementation of the child class.
     *
     * @param _key  The key which is used to identify the data object. The key is always the name of the method which was called on the Proxy interface.
     * @param _data The data which should be saved
     * @return  Returns true if the saving operation was successful.
     */
    boolean save( String _key, PersistData _data );

    /**
     * Loads the data which are tied to the given key
     *
     * @param _key The key which identifies the data object. The key is always the name of the method which was called on the Proxy interface.
     * @return When data were found, the method returns a valid {@link PersistData} object.
     *         If there is no data available, it simply returns null
     */
    PersistData load( String _key );

    /**
     *
     * @param _key The key which identifies the data object. The key is always the name of the method which was called on the Proxy interface.
     * @return
     */
    long getTimestampFromKey( String _key );

}

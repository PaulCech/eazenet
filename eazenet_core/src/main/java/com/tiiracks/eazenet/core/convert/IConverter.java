package com.tiiracks.eazenet.core.convert;

/**
 * Created by Paul Cech on 15.06.16.
 */
public interface IConverter {

    <T> T convertToObject( byte[] data , Class<T> _class);

    <T> byte[] convertToByte( T _object );
}

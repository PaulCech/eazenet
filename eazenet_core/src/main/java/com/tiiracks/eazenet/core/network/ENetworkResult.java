package com.tiiracks.eazenet.core.network;

/**
 * Created by Paul Cech on 05.07.16.
 */
public enum ENetworkResult {
    AVAILABLE,
    ERROR,
    UNREACHABLE
}

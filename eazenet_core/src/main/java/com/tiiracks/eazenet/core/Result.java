package com.tiiracks.eazenet.core;

import com.tiiracks.eazenet.core.convert.EConvertResult;
import com.tiiracks.eazenet.core.network.ENetworkResult;
import com.tiiracks.eazenet.core.persist.EPersistResult;

/**
 * Created by Paul Cech on 16.06.16.
 */
public class Result {

    // ----------------------------------------------------------------------------
    // Member
    // ----------------------------------------------------------------------------

    private static final String LOG_TAG = Result.class.getSimpleName();

    private EPersistResult mPersistResult;
    private ENetworkResult mNetworkResult;
    private EConvertResult mConvertResult;

    // ----------------------------------------------------------------------------
    // Constructor
    // ----------------------------------------------------------------------------

    public Result() {
    }

    public Result( ENetworkResult _networkResult, EPersistResult _persistResult, EConvertResult _convertResult ) {
        mNetworkResult = _networkResult;
        mPersistResult = _persistResult;
        mConvertResult = _convertResult;
    }

    // ----------------------------------------------------------------------------
    // Public methods
    // ----------------------------------------------------------------------------


    public ENetworkResult getNetworkResult() {
        return mNetworkResult;
    }

    public void setNetworkResult( ENetworkResult _networkResult ) {
        mNetworkResult = _networkResult;
    }

    public EPersistResult getPersistResult() {
        return mPersistResult;
    }

    public void setPersistResult( EPersistResult _persistResult ) {
        mPersistResult = _persistResult;
    }

    public EConvertResult getConvertResult() {
        return mConvertResult;
    }

    public void setConvertResult( EConvertResult _convertResult ) {
        mConvertResult = _convertResult;
    }

    // ----------------------------------------------------------------------------
    // Private methods
    // ----------------------------------------------------------------------------

    // ----------------------------------------------------------------------------
    // Listener
    // ----------------------------------------------------------------------------


}

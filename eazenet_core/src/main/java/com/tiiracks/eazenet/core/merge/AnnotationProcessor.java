package com.tiiracks.eazenet.core.merge;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Map;

/**
 * Created by Paul Cech on 03.08.16.
 */
public class AnnotationProcessor {

    // ----------------------------------------------------------------------------
    // Member
    // ----------------------------------------------------------------------------

    private static final String LOG_TAG = AnnotationProcessor.class.getSimpleName();

    // ----------------------------------------------------------------------------
    // Public methods
    // ----------------------------------------------------------------------------

    public static boolean isCollection( Object ob ) {
        return ob instanceof Collection || ob instanceof Map;
    }

    public static boolean isUpdateable( Class< ? > _dataClass ) {
        return _dataClass.getAnnotation( Updateable.class ) != null;
    }

    public static Field getIdField( Class< ? > _dataClass ) {
        return getCustomField( _dataClass, Id.class );
    }

    public static Field getTimestampField( Class< ? > _dataClass ) {
        return getCustomField( _dataClass, Timestamp.class );
    }

    public static Field getCustomField( Class< ? > _dataClass, Class< ? extends Annotation > _annotationClass ) {
        for ( Field field : _dataClass.getDeclaredFields() ) {
            if ( field.getAnnotation( _annotationClass ) != null ) {
                return field;
            }
        }
        return null;
    }


    // ----------------------------------------------------------------------------
    // Private methods
    // ----------------------------------------------------------------------------

    // ----------------------------------------------------------------------------
    // Listener
    // ----------------------------------------------------------------------------


}

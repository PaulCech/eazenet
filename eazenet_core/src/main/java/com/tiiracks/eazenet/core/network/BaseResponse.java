package com.tiiracks.eazenet.core.network;

/**
 * Created by Paul Cech on 24.06.16.
 */
public class BaseResponse {

    // ----------------------------------------------------------------------------
    // Member
    // ----------------------------------------------------------------------------

    private static final String LOG_TAG = BaseResponse.class.getSimpleName();

    private byte[] mData;

    private Class< ? > mDataClass;

    private String mKey;

    private long mTimestamp;

    private ENetworkResult mNetworkResult;

    // ----------------------------------------------------------------------------
    // Constructor
    // ----------------------------------------------------------------------------

    public BaseResponse() {
    }

    public BaseResponse( byte[] _data, Class< ? > _dataClass, String _key, long _timestamp, ENetworkResult _networkResult ) {
        mData = _data;
        mDataClass = _dataClass;
        mKey = _key;
        mTimestamp = _timestamp;
        mNetworkResult = _networkResult;
    }

    // ----------------------------------------------------------------------------
    // Public methods
    // ----------------------------------------------------------------------------

    public byte[] getData() {
        return mData;
    }

    public void setData( byte[] _data ) {
        mData = _data;
    }

    public Class< ? > getDataClass() {
        return mDataClass;
    }

    public void setDataClass( Class< ? > _dataClass ) {
        mDataClass = _dataClass;
    }

    public String getKey() {
        return mKey;
    }

    public void setKey( String _key ) {
        mKey = _key;
    }

    public long getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp( long _timestamp ) {
        mTimestamp = _timestamp;
    }

    public ENetworkResult getNetworkResult() {
        return mNetworkResult;
    }

    public void setNetworkResult( ENetworkResult _networkResult ) {
        mNetworkResult = _networkResult;
    }

    // ----------------------------------------------------------------------------
    // Private methods
    // ----------------------------------------------------------------------------

    // ----------------------------------------------------------------------------
    // Listener
    // ----------------------------------------------------------------------------


}

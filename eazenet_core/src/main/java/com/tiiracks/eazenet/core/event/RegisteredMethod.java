package com.tiiracks.eazenet.core.event;

import java.lang.reflect.Method;

/**
 * Created by Paul Cech on 07.07.16.
 */
public class RegisteredMethod {

    // ----------------------------------------------------------------------------
    // Member
    // ----------------------------------------------------------------------------

    private static final String LOG_TAG = RegisteredMethod.class.getSimpleName();

    private Method mMethod;
    private EThread mThread;

    // ----------------------------------------------------------------------------
    // Constructor
    // ----------------------------------------------------------------------------

    public RegisteredMethod() {
    }

    public RegisteredMethod( Method _method, EThread _thread ) {
        mMethod = _method;
        mThread = _thread;
    }

    // ----------------------------------------------------------------------------
    // Public methods
    // ----------------------------------------------------------------------------

    public Method getMethod() {
        return mMethod;
    }

    public void setMethod( Method _method ) {
        mMethod = _method;
    }

    public EThread getThread() {
        return mThread;
    }

    public void setThread( EThread _thread ) {
        mThread = _thread;
    }

}

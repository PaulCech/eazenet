package com.tiiracks.eazenet.core.network;

import java.util.ArrayDeque;

/**
 * Created by Paul Cech on 24.06.16.
 */
public class ResponsePool {

    public interface IPoolListener {
        void onResponseAdded( BaseResponse _response );
    }

    // ----------------------------------------------------------------------------
    // Member
    // ----------------------------------------------------------------------------

    private static final String LOG_TAG = ResponsePool.class.getSimpleName();

    private ArrayDeque<BaseResponse > mResponseDeque;

    private IPoolListener mPoolListener;

    // ----------------------------------------------------------------------------
    // Constructor
    // ----------------------------------------------------------------------------

    private ResponsePool() {
        mResponseDeque = new ArrayDeque<>(  );
    }

    // ----------------------------------------------------------------------------
    // Singleton
    // ----------------------------------------------------------------------------

    private static ResponsePool sInstance;

    public static ResponsePool getInstance() {
        if ( sInstance == null ) {
            sInstance = new ResponsePool();
        }
        return sInstance;
    }

    // ----------------------------------------------------------------------------
    // Public methods
    // ----------------------------------------------------------------------------


    public void setPoolListener( IPoolListener _poolListener ) {
        mPoolListener = _poolListener;
    }

    public ArrayDeque< BaseResponse > getResponseDeque() {
        return mResponseDeque;
    }

    public void setResponseDeque( ArrayDeque< BaseResponse > _responseDeque ) {
        mResponseDeque = _responseDeque;
    }

    public void addResponse ( BaseResponse _response ) {

        if ( mPoolListener != null ) {
            mPoolListener.onResponseAdded( _response );
        }
        else {
            mResponseDeque.add( _response );
        }
    }

    public BaseResponse getNext() {
        return mResponseDeque.pop();
    }

    public void remove( BaseResponse _response ) {
        mResponseDeque.remove( _response );
    }

    // ----------------------------------------------------------------------------
    // Listener
    // ----------------------------------------------------------------------------

}

package com.tiiracks.eazenet.convert;

import com.google.gson.Gson;
import com.tiiracks.eazenet.core.convert.IConverter;

/**
 * Created by Paul Cech on 15.06.16.
 */
public class GsonConverter implements IConverter {

    // ----------------------------------------------------------------------------
    // Member
    // ----------------------------------------------------------------------------

    private static final String LOG_TAG = GsonConverter.class.getSimpleName();

    private Gson mGson;

    // ----------------------------------------------------------------------------
    // Constructor
    // ----------------------------------------------------------------------------

    public GsonConverter() {
        mGson = new Gson();
    }

    @Override
    public < T > T convertToObject( byte[] data, Class< T > _class ) {
        String jsonString = new String( data );
        return mGson.fromJson( jsonString, _class );
    }

    @Override
    public < T > byte[] convertToByte( T _object ) {
        return mGson.toJson( _object ).getBytes();
    }

    // ----------------------------------------------------------------------------
    // Public methods
    // ----------------------------------------------------------------------------



    // ----------------------------------------------------------------------------
    // Private methods
    // ----------------------------------------------------------------------------

    // ----------------------------------------------------------------------------
    // Listener
    // ----------------------------------------------------------------------------


}

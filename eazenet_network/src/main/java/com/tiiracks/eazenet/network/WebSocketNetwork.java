package com.tiiracks.eazenet.network;

import com.tiiracks.eazenet.core.network.BaseRequest;
import com.tiiracks.eazenet.core.network.IKeyTimestampCallback;
import com.tiiracks.eazenet.core.network.INetwork;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * Created by Paul Cech on 24.06.16.
 */
public class WebSocketNetwork implements INetwork {

    // ----------------------------------------------------------------------------
    // Member
    // ----------------------------------------------------------------------------

    private static final String LOG_TAG = WebSocketNetwork.class.getSimpleName();

    // ----------------------------------------------------------------------------
    // Constructor
    // ----------------------------------------------------------------------------

    public WebSocketNetwork() {
    }

    // ----------------------------------------------------------------------------
    // Public methods
    // ----------------------------------------------------------------------------

    @Override
    public void setBaseUrl( String _baseUrl ) {

    }

    @Override
    public void setKeyTimestampCallback( IKeyTimestampCallback _callback ) {

    }

    @Override
    public BaseRequest onMethodInvocationCalled( Object _o, Method _method, Object[] _methodParameters ) throws Throwable {

        return generateWebSocketCall();
    }

    @Override
    public void startRequest( BaseRequest _baseRequest ) {

    }

    // ----------------------------------------------------------------------------
    // Private methods
    // ----------------------------------------------------------------------------

    private BaseRequest generateWebSocketCall() {
        return null;
    }

    private void handleInvocation( Object _o, Method _method, Object[] _args ) {

        Annotation[] annotations = _method.getAnnotations();
        Class<?>     returnType  = _method.getReturnType();

        if ( annotations != null && annotations.length == 1 ) {
//            computeMethodAnnotation( annotations[0], _method );
        }
        else {
            throw new IllegalArgumentException( "Only one Annotation is currently supported" );
        }

    }
    // ----------------------------------------------------------------------------
    // Listener
    // ----------------------------------------------------------------------------


}

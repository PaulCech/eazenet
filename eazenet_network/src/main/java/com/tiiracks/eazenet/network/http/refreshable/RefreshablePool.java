package com.tiiracks.eazenet.network.http.refreshable;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Paul Cech on 22.08.16.
 */
public class RefreshablePool {

    // ----------------------------------------------------------------------------
    // Member
    // ----------------------------------------------------------------------------

    private static final String LOG_TAG = RefreshablePool.class.getSimpleName();

    public static final int ONE_SECOND = 1000;

    private ArrayList<RefreshableData> mRefreshableData;

    private Handler                     mHandler;
    private boolean                     mRefreshActivated;

    private IOnRefreshableRequestListener mOnRefreshableRequestListener;

    // ----------------------------------------------------------------------------
    // Constructor
    // ----------------------------------------------------------------------------

    public RefreshablePool() {
        mRefreshableData = new ArrayList<>(  );
        mHandler = new Handler(  );
        mRefreshActivated = false;
    }

    // ----------------------------------------------------------------------------
    // Public methods
    // ----------------------------------------------------------------------------


    public void setOnRefreshableRequestListener( IOnRefreshableRequestListener _onRefreshableRequestListener ) {
        mOnRefreshableRequestListener = _onRefreshableRequestListener;
    }

    public void addNewData( RefreshableData _data ) {
        mRefreshableData.add( _data );

        // First time a recurring request was added -> start loop
        if ( mRefreshableData.size() == 1 ) {
            mRefreshActivated = true;
            startLoop();
        }
    }

    public void clearAll() {
        mRefreshActivated = false;
        mRefreshableData.clear();
    }

    // ----------------------------------------------------------------------------
    // Private methods
    // ----------------------------------------------------------------------------

    private void startLoop() {
        mHandler.postDelayed( new Runnable() {
            @Override
            public void run() {

                if ( mRefreshActivated ) {
                    long currentTime = System.currentTimeMillis();

                    for ( RefreshableData data : mRefreshableData ) {
                        if ( currentTime - data.getLastTimeInitiated() > data.getRequest().getAutoRefreshTime() ) {
                            data.setLastTimeInitiated( currentTime );

                            Runnable requestRunnable = new Runnable() {
                                @Override
                                public void run() {
                                    mOnRefreshableRequestListener.onRefreshableRequest( data );
                                }
                            };

                            if ( Looper.myLooper() == Looper.getMainLooper() ) {
                                Thread requestThread = new Thread( requestRunnable );
                                requestThread.start();
                            } else {
                                requestRunnable.run();
                            }
                        }
                    }

                    mHandler.postDelayed( this, ONE_SECOND );
                }

            }
        }, ONE_SECOND );
    }

    // ----------------------------------------------------------------------------
    // Listener
    // ----------------------------------------------------------------------------


}

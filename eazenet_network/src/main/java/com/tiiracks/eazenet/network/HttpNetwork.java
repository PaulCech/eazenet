package com.tiiracks.eazenet.network;

import android.os.Looper;
import android.util.Log;

import com.tiiracks.eazenet.core.Const;
import com.tiiracks.eazenet.core.network.BaseRequest;
import com.tiiracks.eazenet.core.network.ENetworkResult;
import com.tiiracks.eazenet.core.network.IKeyTimestampCallback;
import com.tiiracks.eazenet.core.network.INetwork;
import com.tiiracks.eazenet.core.network.BaseResponse;
import com.tiiracks.eazenet.core.network.ResponsePool;
import com.tiiracks.eazenet.network.http.HttpRequest;
import com.tiiracks.eazenet.network.http.refreshable.IOnRefreshableRequestListener;
import com.tiiracks.eazenet.network.http.refreshable.RefreshableData;
import com.tiiracks.eazenet.network.http.refreshable.RefreshablePool;

import java.io.IOException;
import java.lang.reflect.Method;

import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Paul Cech on 15.06.16.
 */
public class HttpNetwork implements INetwork {

    // ----------------------------------------------------------------------------
    // Member
    // ----------------------------------------------------------------------------

    private static final String LOG_TAG = HttpNetwork.class.getSimpleName();

    private String                mBaseUrl;
    private IKeyTimestampCallback mKeyTimestampCallback;

    private RefreshablePool       mRefreshablePool;

    private OkHttpClient          mClient;

    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    // ----------------------------------------------------------------------------
    // Constructor
    // ----------------------------------------------------------------------------

    public HttpNetwork() {
        mClient = new OkHttpClient();
        mRefreshablePool = new RefreshablePool();
        mRefreshablePool.setOnRefreshableRequestListener( mOnRefreshableRequestListener );
    }

    // ----------------------------------------------------------------------------
    // Public methods
    // ----------------------------------------------------------------------------

    @Override
    public void setBaseUrl( String _baseUrl ) {
        mBaseUrl = _baseUrl;
    }

    @Override
    public void setKeyTimestampCallback( IKeyTimestampCallback _callback ) {
        mKeyTimestampCallback = _callback;
    }

    @Override
    public BaseRequest onMethodInvocationCalled( Object _o, Method _method, Object[] _methodParameters ) throws Throwable {
        return new HttpRequest.Builder( _method, _methodParameters, mBaseUrl ).build();
    }

    @Override
    public void startRequest( BaseRequest _baseRequest ) {
        Runnable requestRunnable = new Runnable() {
            @Override
            public void run() {
                handleRequest( ( HttpRequest ) _baseRequest );
            }
        };

        if ( Looper.myLooper() == Looper.getMainLooper() ) {

            // Every response handling is done in its own thread. When the work is done,
            // the EventPool automatically sends the result to the subscribed thread.
            Thread responseThread = new Thread( requestRunnable );
            responseThread.start();
        } else {

            // Run the runnable in the current thread, as we are already on different thread than
            // the main thread.
            requestRunnable.run();
        }
    }

    // ----------------------------------------------------------------------------
    // Public methods
    // ----------------------------------------------------------------------------

    public void stopAllPendingRefreshableRequests() {
        mRefreshablePool.clearAll();
    }

    // ----------------------------------------------------------------------------
    // Private methods
    // ----------------------------------------------------------------------------

    private void handleRequest( HttpRequest _request ) {

        String url = mBaseUrl + _request.getEndpointUrl();

        Request.Builder builder = new Request.Builder()
                .url(url);

        if ( !_request.isTimestampSet() ) {
            long timestamp = mKeyTimestampCallback.getTimestampFromKey( _request.getKey() );
            // TODO: inject to Last-Modified-Header
        }

        if ( _request.getHttpMethod().equals( "POST" ) ) {

            if ( _request.getPayload() == null ) {
                throw new IllegalArgumentException( "When sending a post message to the server, a payload needs to be present." );
            }
            RequestBody body = RequestBody.create(JSON, ( byte[] ) _request.getPayload() );
            builder.post( body );
        }

        Request okHttpRequest = builder.build();

        if ( _request.isAutoRefreshable() ) {
            // Add recurring request to RefreshablePool
            RefreshableData data = new RefreshableData( okHttpRequest, _request );
            data.setLastTimeInitiated( System.currentTimeMillis() );
            mRefreshablePool.addNewData( data );
        }

        handleResponse( okHttpRequest, _request );
    }

    private void handleResponse( Request _okHttpRequest, HttpRequest _request ) {
        Call call = mClient.newCall(_okHttpRequest);
        Response networkResponse = null;

        try {
            networkResponse = call.execute();
        } catch ( IOException _e ) {
            _e.printStackTrace();
        }

        if ( networkResponse != null && networkResponse.isSuccessful() ) {
            // When no last-modified header was delivered from the server,
            // check in the object merger for timestamp and before set it to -1
            long         newTimestamp = Const.UNSET_TIMESTAMP;
            byte[]       byteBody = new byte[ 0 ];

            try {
                byteBody = networkResponse.body().bytes();
            } catch ( IOException _e ) {
                _e.printStackTrace();
            }

            BaseResponse response     = new BaseResponse( byteBody, _request.getReturnType(), _request.getKey(), newTimestamp, ENetworkResult.AVAILABLE );
            ResponsePool.getInstance().addResponse( response );
        } else {
            BaseResponse response     = new BaseResponse( null, _request.getReturnType(), _request.getKey(), 0L, ENetworkResult.ERROR );
            ResponsePool.getInstance().addResponse( response );
        }
    }

    // ----------------------------------------------------------------------------
    // Listener
    // ----------------------------------------------------------------------------

    private IOnRefreshableRequestListener mOnRefreshableRequestListener = new IOnRefreshableRequestListener() {
        @Override
        public void onRefreshableRequest( RefreshableData _data ) {
            // TODO: When using a refreshable request, how do we handle changing timestamps in the url over time?
            handleResponse( _data.getOkHttpRequest() , _data.getRequest() );
        }
    };

}

package com.tiiracks.eazenet.network.http.refreshable;

import com.tiiracks.eazenet.network.http.HttpRequest;

import okhttp3.Call;
import okhttp3.Request;

/**
 * Created by Paul Cech on 23.08.16.
 */
public class RefreshableData {

    // ----------------------------------------------------------------------------
    // Member
    // ----------------------------------------------------------------------------

    private static final String LOG_TAG = RefreshableData.class.getSimpleName();

    private Request mOkHttpRequest;

    private HttpRequest mRequest;

    private long        mLastTimeInitiated;

    // ----------------------------------------------------------------------------
    // Constructor
    // ----------------------------------------------------------------------------

    public RefreshableData() {
    }

    public RefreshableData( Request _okHttpRequest, HttpRequest _request ) {
        mOkHttpRequest = _okHttpRequest;
        mRequest = _request;
    }

    // ----------------------------------------------------------------------------
    // Public methods
    // ----------------------------------------------------------------------------

    public Request getOkHttpRequest() {
        return mOkHttpRequest;
    }

    public void setOkHttpRequest( Request _okHttpRequest ) {
        mOkHttpRequest = _okHttpRequest;
    }

    public HttpRequest getRequest() {
        return mRequest;
    }

    public void setRequest( HttpRequest _request ) {
        mRequest = _request;
    }

    public long getLastTimeInitiated() {
        return mLastTimeInitiated;
    }

    public void setLastTimeInitiated( long _lastTimeInitiated ) {
        mLastTimeInitiated = _lastTimeInitiated;
    }

    // ----------------------------------------------------------------------------
    // Private methods
    // ----------------------------------------------------------------------------

    // ----------------------------------------------------------------------------
    // Listener
    // ----------------------------------------------------------------------------


}

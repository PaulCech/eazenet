package com.tiiracks.eazenet.network.http.refreshable;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Paul Cech on 28.06.16.
 *
 * Activates a ping behaviour for a REST endpoint. For example a news endpoint, which should be
 * refreshed automatically every one minute.
 */
@Target( ElementType.METHOD )
@Retention( RetentionPolicy.RUNTIME )
@Documented
public @interface Refreshable {

    /**
     * The time in which the request is periodically started.
     * refreshTime is handled in milliseconds
     * @return
     */
    int refreshTime() default 60000;

}

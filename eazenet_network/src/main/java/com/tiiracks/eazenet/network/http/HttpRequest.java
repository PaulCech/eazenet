package com.tiiracks.eazenet.network.http;

import android.util.Log;

import com.tiiracks.eazenet.core.api.Payload;
import com.tiiracks.eazenet.core.network.BaseRequest;
import com.tiiracks.eazenet.network.http.refreshable.Refreshable;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * Created by Paul Cech on 28.06.16.
 */
public class HttpRequest extends BaseRequest {

    // ----------------------------------------------------------------------------
    // Member
    // ----------------------------------------------------------------------------

    private static final String LOG_TAG = HttpRequest.class.getSimpleName();

    private String     mBaseUrl;
    private String     mEndpointUrl;
    private String     mHttpMethod;
    private Class< ? > mReturnType;
    private boolean    mIsAutoRefreshable;
    private int        mAutoRefreshTime;

    // ----------------------------------------------------------------------------
    // Constructor
    // ----------------------------------------------------------------------------

    public HttpRequest( Builder _builder ) {
        mBaseUrl = _builder.mBaseUrl;
        mEndpointUrl = _builder.mEndpointUrl;
        mHttpMethod = _builder.mHttpMethod;
        mReturnType = _builder.mReturnType;
        super.mPayload = _builder.mPayload;
        super.mPayloadClass = _builder.mPayloadClass;
        mIsAutoRefreshable = _builder.mIsAutoRefreshable;
        mAutoRefreshTime = _builder.mAutoRefreshTime;
        super.mKey = mBaseUrl + mEndpointUrl;
    }

    // ----------------------------------------------------------------------------
    // Public methods
    // ----------------------------------------------------------------------------


    public String getBaseUrl() {
        return mBaseUrl;
    }

    public void setBaseUrl( String _baseUrl ) {
        mBaseUrl = _baseUrl;
    }

    public String getEndpointUrl() {
        return mEndpointUrl;
    }

    public String getHttpMethod() {
        return mHttpMethod;
    }

    public Class< ? > getReturnType() {
        return mReturnType;
    }

    public boolean isAutoRefreshable() {
        return mIsAutoRefreshable;
    }

    public int getAutoRefreshTime() {
        return mAutoRefreshTime;
    }

    @Override
    public String toString() {
        return "HttpRequest{" +
                "mBaseUrl='" + mBaseUrl + '\'' +
                ", mEndpointUrl='" + mEndpointUrl + '\'' +
                ", mHttpMethod='" + mHttpMethod + '\'' +
                ", mReturnType=" + mReturnType +
                ", mIsAutoRefreshable=" + mIsAutoRefreshable +
                ", mAutoRefreshTime=" + mAutoRefreshTime +
                '}';
    }


    // ----------------------------------------------------------------------------
    // Private methods
    // ----------------------------------------------------------------------------

    // ----------------------------------------------------------------------------
    // Builder
    // ----------------------------------------------------------------------------

    public static final class Builder {

        private Method   mMethod;
        private Object[] mMethodParameters;

        private String     mBaseUrl;
        private String     mEndpointUrl;
        private String     mHttpMethod;
        private Class< ? > mReturnType;
        private Object     mPayload;
        private Class< ? > mPayloadClass;
        private boolean    mIsAutoRefreshable;
        private int        mAutoRefreshTime;

        public Builder( Method _method, Object[] _methodParameters, String _baseUrl ) {
            mBaseUrl = _baseUrl;
            mMethod = _method;
            mMethodParameters = _methodParameters;
        }

        public HttpRequest build() {
            mReturnType = mMethod.getReturnType();
            Annotation[] annotations = mMethod.getAnnotations();
            Annotation[][] parameterAnnotations = mMethod.getParameterAnnotations();

            if ( annotations != null ) {
                for ( Annotation annotation : annotations ) {
                    computeMethodAnnotation( annotation, mMethod );
                }
            } else {
                throw new IllegalArgumentException( "Please declare a http method with a valid server url" );
            }

            if ( mMethodParameters != null && mMethodParameters.length > 0 ) {
                computeMethodParameters( parameterAnnotations, mMethodParameters );
            }

            return new HttpRequest( this );
        }


        private void computeMethodAnnotation( Annotation _annotation, Method _method ) {
            if ( _annotation instanceof GET ) {
                mEndpointUrl = ( ( GET ) _annotation ).value();
                mHttpMethod = "GET";
            } else if ( _annotation instanceof POST ) {
                mEndpointUrl = ( ( POST ) _annotation ).value();
                mHttpMethod = "POST";
            } else if ( _annotation instanceof Refreshable ) {
                mIsAutoRefreshable = true;
                mAutoRefreshTime = ( ( Refreshable ) _annotation ).refreshTime();
            }

            // When building the url endpoint, check if a timestamp was injected to the url as a
            // GET Parameter. If this is true, set the BaseRequest.mTimestampSet to true so no
            // further timestamp injections are done.
        }

        private void computeMethodParameters( Annotation[][] _parameterAnnotations, Object[] _methodParameters ) {

            // Check if a payload is present and inject timestamp if needed.
            if ( _methodParameters != null && _methodParameters.length > 0 ) {
                for ( int i = 0; i < _methodParameters.length; i++ ) {
                    Object  parameter = _methodParameters[ i ];
                    Annotation[] annotations = _parameterAnnotations[i];
                    for ( Annotation annotation : annotations ) {
                        if (annotation instanceof Payload ) {
                            mPayload = parameter;
                            mPayloadClass = parameter.getClass();
                        }
                    }
                }
            }
        }
    }

}

package com.tiiracks.eazenet.network.http;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Paul Cech on 15.08.16.
 */
@Target( ElementType.METHOD )
@Retention( RetentionPolicy.RUNTIME )
@Documented
public @interface TimestampParameter {
    // Can be used over an api method to show, that the timestamp is registered through the
    // Url parameter on a server.

    String value() default "";
}

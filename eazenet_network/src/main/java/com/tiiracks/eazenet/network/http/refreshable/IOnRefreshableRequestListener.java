package com.tiiracks.eazenet.network.http.refreshable;

/**
 * Created by Paul Cech on 23.08.16.
 */
public interface IOnRefreshableRequestListener {

    void onRefreshableRequest( RefreshableData _data );

}

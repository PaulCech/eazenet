package com.tiiracks.eazenet.persist;

import com.tiiracks.eazenet.core.persist.EPersistResult;
import com.tiiracks.eazenet.core.persist.IPersist;
import com.tiiracks.eazenet.core.persist.PersistData;

/**
 * Created by Paul Cech on 05.07.16.
 */
public class FilePersist implements IPersist {

    // ----------------------------------------------------------------------------
    // Member
    // ----------------------------------------------------------------------------

    private static final String LOG_TAG = FilePersist.class.getSimpleName();

    // ----------------------------------------------------------------------------
    // Constructor
    // ----------------------------------------------------------------------------

    public FilePersist() {
    }

    // ----------------------------------------------------------------------------
    // Public methods
    // ----------------------------------------------------------------------------

    @Override
    public boolean save( String _key, PersistData _data ) {
        return false;
    }

    @Override
    public PersistData load( String _key ) {
        return null;
    }

    @Override
    public long getTimestampFromKey( String _key ) {
        return 0;
    }


    // ----------------------------------------------------------------------------
    // Private methods
    // ----------------------------------------------------------------------------

    // ----------------------------------------------------------------------------
    // Listener
    // ----------------------------------------------------------------------------


}

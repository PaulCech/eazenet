package com.tiiracks.eazenet.persist;

import com.tiiracks.eazenet.core.Const;
import com.tiiracks.eazenet.core.persist.EPersistResult;
import com.tiiracks.eazenet.core.persist.IPersist;
import com.tiiracks.eazenet.core.persist.PersistData;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Paul Cech on 15.06.16.
 */
public class RamPersist implements IPersist {

    // ----------------------------------------------------------------------------
    // Member
    // ----------------------------------------------------------------------------

    private static final String LOG_TAG = RamPersist.class.getSimpleName();

    private Map< String, PersistData > mPersistDataMap;

    // ----------------------------------------------------------------------------
    // Constructor
    // ----------------------------------------------------------------------------

    public RamPersist() {
        mPersistDataMap = new HashMap<>(  );
    }

    // ----------------------------------------------------------------------------
    // Public methods
    // ----------------------------------------------------------------------------

    @Override
    public boolean save( String _key, PersistData _data ) {

        if ( _data == null || _data.getData() == null ) {
            // TODO: Use Exception or Error return value?
            throw new IllegalStateException( "The data object and its contents should not be null." );
        }

        mPersistDataMap.put( _key, _data );

        return true;
    }

    @Override
    public PersistData load( String _key ) {

        if ( mPersistDataMap.containsKey( _key ) ) {
            return mPersistDataMap.get( _key );
        }
        return null;
    }

    @Override
    public long getTimestampFromKey( String _key ) {
        long result = Const.UNSET_TIMESTAMP;
        PersistData data = load( _key );

        if ( data != null ) {
            result = data.getTimestamp();
        }

        return result;
    }

    // ----------------------------------------------------------------------------
    // Private methods
    // ----------------------------------------------------------------------------

    // ----------------------------------------------------------------------------
    // Listener
    // ----------------------------------------------------------------------------


}

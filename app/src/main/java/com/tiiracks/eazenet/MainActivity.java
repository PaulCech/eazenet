package com.tiiracks.eazenet;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import com.tiiracks.eazenet.convert.GsonConverter;
import com.tiiracks.eazenet.core.EazeNet;
import com.tiiracks.eazenet.core.event.EThread;
import com.tiiracks.eazenet.core.event.EazeCallback;
import com.tiiracks.eazenet.core.Result;
import com.tiiracks.eazenet.data.DummyResult;
import com.tiiracks.eazenet.data.TimestampReq;
import com.tiiracks.eazenet.data.WrapperResult;
import com.tiiracks.eazenet.network.HttpNetwork;
import com.tiiracks.eazenet.persist.RamPersist;

public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    private EazeNet mEazeNet;
    private Api     mApi;

    private TextView mTextView;
    private String mText;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        mTextView = ( TextView ) findViewById( R.id.text );
        mText = "";

        Button button = ( Button ) findViewById( R.id.button );
        button.setOnClickListener( _v -> {
//            mApi.test();
            mApi.testList();
//            mApi.testWrapped();
//            mApi.postTest( new TimestampReq( -1 ) );
        } );

        EazeNet.Builder builder = new EazeNet.Builder();
        builder.setNetwork( new HttpNetwork() );
        builder.setPersistence( new RamPersist() );
        builder.setConverter( new GsonConverter() );
        builder.setBaseUrl( "http://192.168.0.103:3000/" );

        mEazeNet = builder.create();
        mApi = mEazeNet.create( Api.class );
    }

    @Override
    protected void onResume() {
        super.onResume();
        mEazeNet.register( this );
    }

    @Override
    protected void onPause() {
        mEazeNet.unregister( this );
        super.onPause();
    }

    // ----------------------------------------------------------------------------
    // Listener
    // ----------------------------------------------------------------------------

    @EazeCallback( thread = EThread.MAIN )
    public void onDummyResult( DummyResult _dummyResult, Result _result ) {
        Log.d( LOG_TAG, "onDummyResult " + _dummyResult.toString() );
        mText += "\n" + _dummyResult.toString();
        mTextView.setText( mText );
    }

    @EazeCallback( thread = EThread.MAIN )
    public void onDummyListResult( DummyResult[] _dummyResult, Result _result ) {

        for ( int i = 0; i < _dummyResult.length; i++ ) {
            Log.d( LOG_TAG, i + " " + _dummyResult[i] );
        }
    }

    @EazeCallback( thread = EThread.MAIN )
    public void onWrappedResult( WrapperResult _wrapResult, Result _result ) {

        Log.d( LOG_TAG, "onWrappedResult " + _wrapResult.getTimestamp() );
        for ( DummyResult result : _wrapResult.getResults() ) {
            Log.d( LOG_TAG, result.toString() );
        }
    }

}

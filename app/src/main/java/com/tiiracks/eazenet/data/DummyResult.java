package com.tiiracks.eazenet.data;

import com.google.gson.annotations.SerializedName;
import com.tiiracks.eazenet.core.merge.*;

/**
 * Created by Paul Cech on 15.06.16.
 */
@Updateable
public class DummyResult {

    // ----------------------------------------------------------------------------
    // Member
    // ----------------------------------------------------------------------------

    private static final String LOG_TAG = DummyResult.class.getSimpleName();

    @SerializedName( "id" )
    @Id
    private long mId;

    @SerializedName( "result" )
    private String mResult;

    @SerializedName( "timestamp" )
    @Timestamp
    private long mTimestamp;

    // ----------------------------------------------------------------------------
    // Constructor
    // ----------------------------------------------------------------------------

    public DummyResult() {
    }

    public DummyResult( long _id, String _result, long _timestamp ) {
        mId = _id;
        mResult = _result;
        mTimestamp = _timestamp;
    }

    // ----------------------------------------------------------------------------
    // Public methods
    // ----------------------------------------------------------------------------


    public long getId() {
        return mId;
    }

    public void setId( long _id ) {
        mId = _id;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult( String _result ) {
        mResult = _result;
    }

    public long getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp( long _timestamp ) {
        mTimestamp = _timestamp;
    }

    @Override
    public String toString() {
        return "DummyResult{" +
                "mId=" + mId +
                ", mResult='" + mResult + '\'' +
                ", mTimestamp=" + mTimestamp +
                '}';
    }
}

package com.tiiracks.eazenet.data;

import com.google.gson.annotations.SerializedName;
import com.tiiracks.eazenet.core.merge.Mixable;
import com.tiiracks.eazenet.core.merge.Timestamp;
import com.tiiracks.eazenet.core.merge.Updateable;

/**
 * Created by Paul Cech on 02.08.16.
 */
@Updateable
public class WrapperResult {

    // ----------------------------------------------------------------------------
    // Member
    // ----------------------------------------------------------------------------

    private static final String LOG_TAG = WrapperResult.class.getSimpleName();


    @SerializedName( "timestamp" )
    @Timestamp
    private long mTimestamp;

    @SerializedName( "results" )
    @Mixable
    private DummyResult[] mResults;


    // ----------------------------------------------------------------------------
    // Constructor
    // ----------------------------------------------------------------------------

    public WrapperResult() {
    }

    public WrapperResult( long _timestamp, DummyResult[] _results ) {
        mTimestamp = _timestamp;
        mResults = _results;
    }

    // ----------------------------------------------------------------------------
    // Public methods
    // ----------------------------------------------------------------------------

    public long getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp( long _timestamp ) {
        mTimestamp = _timestamp;
    }

    public DummyResult[] getResults() {
        return mResults;
    }

    public void setResults( DummyResult[] _results ) {
        mResults = _results;
    }


    // ----------------------------------------------------------------------------
    // Private methods
    // ----------------------------------------------------------------------------

    // ----------------------------------------------------------------------------
    // Listener
    // ----------------------------------------------------------------------------


}

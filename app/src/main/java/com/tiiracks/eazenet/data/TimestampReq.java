package com.tiiracks.eazenet.data;

import com.google.gson.annotations.SerializedName;
import com.tiiracks.eazenet.core.merge.Timestamp;

/**
 * Created by Paul Cech on 15.08.16.
 */
public class TimestampReq {

    // ----------------------------------------------------------------------------
    // Member
    // ----------------------------------------------------------------------------

    private static final String LOG_TAG = TimestampReq.class.getSimpleName();

    @SerializedName( "timestamp" )
    @Timestamp
    private long mTimestamp;

    // ----------------------------------------------------------------------------
    // Constructor
    // ----------------------------------------------------------------------------

    public TimestampReq() {
    }

    public TimestampReq( long _timestamp ) {
        mTimestamp = _timestamp;
    }

    // ----------------------------------------------------------------------------
    // Public methods
    // ----------------------------------------------------------------------------


    public long getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp( long _timestamp ) {
        mTimestamp = _timestamp;
    }

    @Override
    public String toString() {
        return "TimestampReq{" +
                "mTimestamp=" + mTimestamp +
                '}';
    }
}

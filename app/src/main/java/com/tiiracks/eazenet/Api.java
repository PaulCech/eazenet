package com.tiiracks.eazenet;

import com.tiiracks.eazenet.core.api.Payload;
import com.tiiracks.eazenet.data.DummyResult;
import com.tiiracks.eazenet.data.TimestampReq;
import com.tiiracks.eazenet.data.WrapperResult;
import com.tiiracks.eazenet.network.http.GET;
import com.tiiracks.eazenet.network.http.POST;
import com.tiiracks.eazenet.network.http.refreshable.Refreshable;

/**
 * Created by Paul Cech on 21.06.16.
 */
public interface Api {

    @GET( "simple" )
    DummyResult test ();

    @POST( "post" )
    DummyResult postTest ( @Payload TimestampReq _timestampReq );

    @Refreshable( refreshTime = 6000 )
    @GET( "list" )
    DummyResult[] testList ();

    @GET( "wrapped" )
    WrapperResult testWrapped ();
}
